param ([string] $repository)

if ($repository) {
  $solutionFile = Get-ChildItem -Path $repository -Recurse -Filter *.sln | Select-Object -first 1
  $MSBUILD_PATH = $Env:MSBUILD_PATH
  $msBuildExe = "$MSBUILD_PATH\MSBUILD.exe"

  $buildToolsPath = $Env:BUILD_TOOLS_PATH
  $nugetExe = "$buildToolsPath\nuget.exe"
  $nugetUrl = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"

  Write-Host "Pulling Latest" -foregroundcolor green  
  git pull
  
  if(!(test-path $buildToolsPath))
  {
    Write-Host "Downloading nuget.exe from $nugetUrl to $nugetExe" -foregroundcolor green
    New-Item -ItemType Directory -Force -Path $buildToolsPath
	  
	if(!(test-path $nugetExe))
	{
	  (New-Object System.Net.WebClient).DownloadFile($nugetUrl, $nugetExe)
	}
  }
  
  Write-Host "Restoring NuGet packages" -foregroundcolor green
    & "$($nugetExe)" restore "$($repository)"
  
  Write-Host "Cleaning $($repository)" -foregroundcolor green
    & "$($msBuildExe)" "$($repository)" /t:Clean /m
  
  Write-Host "Building $($path)" -foregroundcolor green
        & "$($msBuildExe)" "$($path)" /t:Build /m
  
  if ($solutionFile) {
    Write-Host "Building $($path)" -foregroundcolor green
        & "$($msBuildExe)" "$($solutionFile.FullName)" /t:Build /p:Configuration=Release /m
  }
}
