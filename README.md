# source-tree-custom-actions

Handy Source Tree Custom Actions for Visual Studio workflows

- sourcetree-build-respository
    - Pull Latest
    - Restore Packages, Clean, Build if repository has a .sln file
    - This will download a copy of the latest nuget.exe from nuget.org
- sourcetree-open-respository
    - Open the .sln file in Visual Studio if it exists
- sourcetree-open-repository-vscode
    - Opens the respository directory in VSCode
    


### Assumptions: 
  - You're using Windows OS
  - Source Tree is installed
  - Visual Studio Install Path: 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional'
  - VSCode is installed


### Setup
  1. Clone this repo to your local machine.
  2. Open Source Tree and navigate to `Tools > Options > Custom Actions > Click [Add]`.
  3. Add a Menu caption ie `Build Solution`. 
  4. Set the `Script to run:` to the `sourcetree-build-repository.cmd` local path.
  5. Set `Parameters:` to `$REPO`
  6. Click `[OK]` button.
  7. #Profit



### Fork this!
Please feel free to fork this or add on to this collection. 

